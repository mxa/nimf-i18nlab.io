---
layout: page
title: Support
permalink: /support/
---

## Documents

If you have trouble using Nimf, see [documents](https://nimf-i18n.gitlab.io/docs).

## Report a bug

If you have a problem with Nimf, please report a bug.

[https://gitlab.com/nimf-i18n/nimf/issues/new](https://gitlab.com/nimf-i18n/nimf/issues/new)
