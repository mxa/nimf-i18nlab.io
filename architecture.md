---
layout: page
title: Architecture
permalink: /architecture/
---

Nimf is an input method framework which has a **module-based client-server**
architecture in which an application acts as a client and communicates
**synchronously** with the Nimf server via a Unix socket.

```
GTK2 Application                         +------------------------ nimf -------------------+
+ GtkIMContext                           |   NimfServer signal: "engine-changed"           |
  + im-nimf-gtk2.so -----+               |                                                 |
                         |               | +---- service modules ---+                      |
GTK3 Application         |               | | implement NimfService  |                      |
+ GtkIMContext           |   UNIX socket | |                        |                      |
  + im-nimf-gtk3.so - NimfIM --------------- nimf-nim.so            |                      |
                       | |               | |     +-- NimfNimIC      ----+                  |
Qt4 Application        | |         +-------- nimf-xim.so            | NimfServiceIC ----+  |
+ QInputContext        | |         |     | |     +-- NimfXimIC      ---+  |  |          |  |
  + im-nimf-qt4.so ----+ |         |  +----- nimf-wayland.so        |     |  |          |  |
                         |         |  |  | |     +-- NimfWaylandIC  ------+  |          |  |
Qt5 Application          |         |  |  | |                        |        |          |  |
+ QPlatformInputContext  |         |  |  | | nimf-preedit-window.so - NimfPreeditable   |  |
  + im-nimf-qt5.so ------+         |  |  | | nimf-candidate.so      - NimfCandidatable -+  |
                                   |  |  | | nimf-indicator.so      - "engine-changed"  |  |
X Application                      |  |  | +------------------------+                   |  |
+ libX11 ------------ X Server ----+  |  |                                              |  |
                                      |  | +------ engine modules ---+                  |  |
Wayland Application                   +  | | nimf-anthy.so           ------+            |  |
+ libwayland-client - Wayland compositor | | nimf-libhangul.so       ----+ |            |  |
                                         | | nimf-m17n-*.so          - NimfEngine ------+  |
                                         | | nimf-rime.so            -----+ |              |
                                         | | nimf-system-keyboard.so -------+              |
                                         | +-------------------------+                     |
                                         +-------------------------------------------------+
```

Nimf uses the `lock.pid` file to create a single-instance daemon and uses the
`socket` file for security reasons.
These files are located in the `$XDG_RUNTIME_DIR/nimf` directory.

```
/run/user/<uid>/nimf/lock.pid
/run/user/<uid>/nimf/socket
```
