---
layout: page
title: Installation
permalink: /installation/
---

[한국어 버전 (Korean version)](/ko/installation/)

## Ubuntu, Kubuntu

```
sudo add-apt-repository ppa:hodong/nimf
sudo apt update
```

To use all input methods, install the following packages.

```
sudo apt install nimf nimf-anthy nimf-libhangul nimf-m17n nimf-rime
```

On the other hand, if you want to use only the Korean input method, please
install the following packages.

```
sudo apt install nimf nimf-libhangul
```

## Arch Linux, Manjaro

```
wget https://gitlab.com/nimf-i18n/nimf/raw/master/archlinux/PKGBUILD
sudo pacman -S binutils
makepkg -si PKGBUILD
```

## Fedora, OpenSUSE

```
sudo yum install yum-plugin-copr
sudo yum copr enable hodong/nimf
sudo yum install nimf
```

## CentOS

```
sudo yum install epel-release yum-plugin-copr
sudo yum copr enable hodong/nimf
sudo yum install nimf
```

## Setup environment variables

### nimf-settings

After installing nimf, run `nimf-settings` and turn on the
`Setup environment variables` option.  
*If you want to use other input methods, you must turn off the
`Setup environment variables` option.*

![Setup environment variables](/images/setup-environment-variables.png)

### Ubuntu, Kubuntu

```
im-config -n nimf
```

### Fedora, OpenSUSE, CentOS

```
imsettings-switch nimf
```

And then log in again and you can use nimf right away.
