---
layout: post
title: "nimf-chewing will be dropped in the next version"
---

We can input Zhuyin with RIME schema data for Bopomofo.  
To reduce maintenance time, nimf-chewing will be dropped in the next version.  
Use nimf-rime instead of nimf-chewing.