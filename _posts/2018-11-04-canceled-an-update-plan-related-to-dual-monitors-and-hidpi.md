---
layout: post
title: "HiDPI, 듀얼 모니터 관련 업데이트 계획 취소"
---

사람들 반응이 좋지 않아 듀얼 모니터, HiDPI 관련 업데이트 계획을 취소하기로
했습니다.

[https://kldp.org/node/160464](https://kldp.org/node/160464)

감사합니다.