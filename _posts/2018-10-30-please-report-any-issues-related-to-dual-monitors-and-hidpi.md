---
layout: post
title: "듀얼 모니터, HiDPI 관련 이슈를 모두 보고해 주세요"
---

안녕하세요.

듀얼 모니터, HiDPI 이슈를 해결하기 위해 컴퓨터를 구입하기로 했습니다.

[#44 Wrong cursor location on HiDPI; 레티나 디스플레이에서 특수문자 입력창 생성 위치](https://gitlab.com/nimf-i18n/nimf/issues/44)

[#83 한국어 한자 입력 시 조판 위치 in GNOME](https://gitlab.com/nimf-i18n/nimf/issues/83)

이 이슈들을 해결한 후에 모니터를 중고로 처분할 계획입니다. 이후 관련 이슈가
올라오면 비용 문제 때문에 또 다시 확인하려면 오랜 시간이 걸립니다.

따라서 듀얼 모니터, HiDPI 관련하여 아직 보고하지 않은 이슈나 알고 계신 이슈가
있다면 모두 보고해 주시기 바랍니다.

장인 정신으로 Nimf 를 개발하고 유지 보수하고 있습니다. 감사합니다.