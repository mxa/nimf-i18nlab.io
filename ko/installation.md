---
layout: page
title: 설치
permalink: /ko/installation/
---

[English version (영어 버전)](/installation/)

## 우분투, 쿠분투

```
sudo add-apt-repository ppa:hodong/nimf
sudo apt update
```

모든 입력 방법을 사용하려면 다음 패키지를 설치하십시오.

```
sudo apt install nimf nimf-anthy nimf-libhangul nimf-m17n nimf-rime
```

반면에, 한국어 입력 방법만 사용하려면 다음 패키지를 설치하십시오.

```
sudo apt install nimf nimf-libhangul
```

## 아치 리눅스, 만자로

```
wget https://gitlab.com/nimf-i18n/nimf/raw/master/archlinux/PKGBUILD
sudo pacman -S binutils
makepkg -si PKGBUILD
```

## 페도라, 오픈수세

```
sudo yum install yum-plugin-copr
sudo yum copr enable hodong/nimf
sudo yum install nimf
```

## CentOS

```
sudo yum install epel-release yum-plugin-copr
sudo yum copr enable hodong/nimf
sudo yum install nimf
```

## 환경 변수 설정

### nimf-settings

nimf 설치하신 후, nimf-settings 를 실행하셔서 `환경 변수 설정` 옵션을 켜주세요.  
*만약 다른 입력기를 사용하시려면 `환경 변수 설정` 옵션을 반드시 꺼주셔야 합니다.*

![환경 변수 설정 옵션](/images/ko/setup-environment-variables.png)

### 우분투, 쿠분투

```
im-config -n nimf
```

### 페도라, 오픈수세, CentOS

```
imsettings-switch nimf
```

그리고 나서 다시 로그인하시면 nimf 를 바로 사용하실 수 있습니다.
