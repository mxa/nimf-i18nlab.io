---
layout: post
title: "끝글자 버그를 고칩시다 3 - scintilla"
date: 2019-06-27 23:17:57 +0900
---

[끝글자 버그를 고칩시다 2 - wxWidgets](/docs/lets-fix-the-last-character-bug-2/) 에 이어서 계속되는 글입니다.

## scintilla 끝글자 버그

geany 에도 끝글자 버그가 있습니다. 이 역시 마우스 클릭할 때 reset 을 하지 않아서 발생하는 버그입니다.  
geany 는 scintilla 를 사용합니다. scintilla 코드를 고치겠습니다.  
패치는 아래와 같습니다. 버튼 클릭할 때, 항상 reset 하는 건 아니고 preedit string 가 있을 때만 im_context 을 reset 합니다.  
이렇게 고치기 쉬운 버그가 바로 끝글자 버그입니다.

[https://sourceforge.net/p/scintilla/bugs/2111/](https://sourceforge.net/p/scintilla/bugs/2111/)  
[https://github.com/geany/geany/pull/2197](https://github.com/geany/geany/pull/2197)

```diff
hodong@debian:~/Downloads/geany_1.33.orig/geany-1.33$ git diff scintilla/gtk/ScintillaGTK.*
diff --git a/scintilla/gtk/ScintillaGTK.cxx b/scintilla/gtk/ScintillaGTK.cxx
index 3e48ed6..75c7169 100644
--- a/scintilla/gtk/ScintillaGTK.cxx
+++ b/scintilla/gtk/ScintillaGTK.cxx
@@ -665,6 +665,8 @@ void ScintillaGTK::Init() {
                caret.period = 0;
        }
 
+       g_signal_connect(PWidget(wMain), "button-press-event", G_CALLBACK(ScintillaGTK::OnButtonPress), NULL);
+
        for (TickReason tr = tickCaret; tr <= tickDwell; tr = static_cast<TickReason>(tr + 1)) {
                timers[tr].reason = tr;
                timers[tr].scintilla = this;
@@ -2154,6 +2156,18 @@ gboolean ScintillaGTK::KeyRelease(GtkWidget *widget, GdkEventKey *event) {
        return FALSE;
 }
 
+gboolean ScintillaGTK::OnButtonPress(GtkWidget *widget, GdkEvent *event, gpointer user_data) {
+       ScintillaGTK *sciThis = FromWidget(widget);
+
+       if (sciThis->im_context) {
+               PreEditString pes(sciThis->im_context);
+               if (strlen(pes.str) > 0)
+                       gtk_im_context_reset(sciThis->im_context);
+       }
+
+       return FALSE;
+}
+
 #if GTK_CHECK_VERSION(3,0,0)
 
 gboolean ScintillaGTK::DrawPreeditThis(GtkWidget *, cairo_t *cr) {
diff --git a/scintilla/gtk/ScintillaGTK.h b/scintilla/gtk/ScintillaGTK.h
index a20f3bc..acb72bb 100644
--- a/scintilla/gtk/ScintillaGTK.h
+++ b/scintilla/gtk/ScintillaGTK.h
@@ -185,6 +185,7 @@ private:
        gboolean KeyThis(GdkEventKey *event);
        static gboolean KeyPress(GtkWidget *widget, GdkEventKey *event);
        static gboolean KeyRelease(GtkWidget *widget, GdkEventKey *event);
+       static gboolean OnButtonPress(GtkWidget *widget, GdkEvent *event, gpointer user_data);
 #if GTK_CHECK_VERSION(3,0,0)
        gboolean DrawPreeditThis(GtkWidget *widget, cairo_t *cr);
        static gboolean DrawPreedit(GtkWidget *widget, cairo_t *cr, ScintillaGTK *sciThis);
```

scintilla 에 끝글자 버그 패치는 수정되어 적용이 되었습니다.  
[https://sourceforge.net/p/scintilla/code/ci/512ec9ab2e7c504e0be2947529e3e104a71bdfc1/](https://sourceforge.net/p/scintilla/code/ci/512ec9ab2e7c504e0be2947529e3e104a71bdfc1/)

---

다음 글에서 계속됩니다. [끝글자 버그를 고칩시다 4 - 결론](/docs/lets-fix-the-last-character-bug-4/)
