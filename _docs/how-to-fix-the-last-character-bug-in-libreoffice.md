---
layout: post
title: "리브레오피스 끝글자 버그 해결 방법"
date: 2019-07-06 16:10:27 +0900
---

리브레오피스는 두가지 모드 등으로 실행될 수 있습니다.

1. X 어플리케이션으로 실행될 때  
입력 방법으로 XIM 이 사용됩니다.  
이 경우 리브레오피스 자체에 끝글자 버그가 발생합니다.  
마우스 클릭할 때 리브레오피스가 입력기를 리셋하지 않아서 끝글자 버그가 발생합니다.  
[https://bugs.documentfoundation.org/show_bug.cgi?id=117008](https://bugs.documentfoundation.org/show_bug.cgi?id=117008)  
[https://ask.libreoffice.org/ko/question/146416/ribeure-opiseu-ggeutgeulja-beogeu/](https://ask.libreoffice.org/ko/question/146416/ribeure-opiseu-ggeutgeulja-beogeu/)

2. GTK 어플리케이션으로 실행될 때  
입력 방법으로 GTK IM 이 사용됩니다. 따라서 끝글자 버그가 발생하지 않습니다.

귀하께서 리브레오피스 끝글자 버그를 겪고 계시다면, 다음과 같은 해결 방법이 있습니다.

1. 데비안, 우분투 계열 배포판을 사용하는 경우  
libreoffice-gnome 또는 libreoffice-gtk 패키지를 설치하시기 바랍니다.

2. 아치리눅스 계열 배포판을 사용하는 경우  
[https://wiki.archlinux.org/index.php/LibreOffice#Theme](https://wiki.archlinux.org/index.php/LibreOffice#Theme) 를 참고하셔서 gtk 테마를
적용하시기 바랍니다.

그 후 끝글자 버그가 없는 nimf 입력기를 사용하시면 되겠습니다.
