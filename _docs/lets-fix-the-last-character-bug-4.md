---
layout: post
title: "끝글자 버그를 고칩시다 4 - 결론"
date: 2019-06-27 23:58:01 +0900
---

[끝글자 버그를 고칩시다 3 - scintilla](/docs/lets-fix-the-last-character-bug-3/) 에 이어서 계속되는 글입니다.

## 결론

scintilla 에 끝글자 버그 패치는 수정되어 적용이 되었습니다.  
[https://sourceforge.net/p/scintilla/code/ci/512ec9ab2e7c504e0be2947529e3e104a71bdfc1/](https://sourceforge.net/p/scintilla/code/ci/512ec9ab2e7c504e0be2947529e3e104a71bdfc1/)  
그런데, 패치 적용하고 fcitx 에서 테스트하면 문제가 있다는 의견이 있어서
패치가 삭제될 가능성이 있습니다. 제가 패치 적용하고 fcitx 에서 테스트할 때는 문제가 없는데,
그러한 작동 방식이 GTK+ 에도 사용되는데 scintilla 에서는 그런 동작을 원치 않는 것 같습니다.
아마도 적용된 커밋이 삭제될 듯 합니다.

그리고 wxWidgets 는 GTK 의 wrapper 입니다. 패치 만들어서 적용해보니 부작용이 자꾸 발생하더군요. 언어별 부작용이 아니라, 이벤트 처리에 대한 부작용을 일컫는 겁니다. 어째서 wxWidgets 에 이러한 끝글자 버그가 발생되는지에 대해서는 아래에 상세하게 코드로 설명해놓았습니다. 이 버그는 nimf 에 있는 옵션으로 회피할 수 없습니다.  
[https://github.com/wxWidgets/wxWidgets/pull/1357](https://github.com/wxWidgets/wxWidgets/pull/1357)  
아무튼 여러 삽질 끝에 최종적으로 코드를 작성했는데,  
[https://github.com/wxWidgets/wxWidgets/pull/1367](https://github.com/wxWidgets/wxWidgets/pull/1367)  
수용할 수 없다는 것으로 이해되는 답변을 받았습니다.

wxWidgets 는 GTK+ 에 대한 wrapper 입니다. 제가 제안한 방식을 수용할 수 없다면, wxWidgets 에 GTK 이벤트 핸들링을 재설계해야 해결할 수 있습니다. 제 입장에서는, 제가 wxWidgets 프로젝트에 대한 권한이 없고 그걸 재설계한다고 해도 수용 여부를 알 수 없는데, 그걸 재설계할 바에는, 만약 필요하다면 가칭 Nimdra GUI Toolkit 을 만드는게 좋겠죠. *([https://gitlab.gnome.org/GNOME/gtk/issues/1934](https://gitlab.gnome.org/GNOME/gtk/issues/1934) 버그가 해결되지 않으면 그게 정말 필요할 수도 있습니다.)*

사용자분들 입장에서는 nimf 만 멀쩡하면 뭐하냐, scintilla, wxWidgets, libreoffice, wine 등의 끝글자 버그는 그대로인데, 땜빵 코드(workaround code)로 해결하는 것은 좋은 해결법이 아니니, 한글 끝글자 버그를 근본적으로 해결해야 하지 않겠느냐 하시는 분들이 당연히 계실텐데, 이러한 사정이 있습니다. 타 응용 어플에 무한정 시간을 소비할 수는 없습니다.

nimf 를 사용하시는 분들은 nimf 에 끝글자 버그를 회피하기 위한 옵션이 있으니 그걸 사용하시기 바랍니다. 회피 옵션 괜히 있는게 아닙니다.

![options-1](/images/options-1.png)

![options-2](/images/options-2.png)

문제를 해결하기 위해 노력해주시고 시간을 내주신 개발자 분들의 의견과 결정을 존중합니다.  
타 응용 어플 끝글자 버그를 고치기 위한 노력은 여기서 끝을 맺겠습니다.

감사합니다.

---

처음 글은 "[끝글자 버그를 고칩시다 1 - ibus](/docs/lets-fix-the-last-character-bug-1/)" 입니다.
