---
layout: post
title: nimf 와 카카오톡 사용하기
date: 2019-04-22 23:32:45 +0900
---

nimf 와 카카오톡(kakaotalk)을 사용하면 글자가 반복되면서 이상하게 입력된다는 보고가
많았습니다. 이는 wine 버그이며 wine 의 입력 스타일을 `root` 입력 스타일로
설정하는 레지스트리를 추가하면 글자가 이상하게 입력되는 문제가 해결됩니다.

`input-style-root.reg` 파일을 생성하고 그 파일에 다음 내용을 넣습니다.

```
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Wine\X11 Driver]
"InputStyle"="root"
```

터미널에서 다음 명령을 하면 레지스트리가 병합됩니다.

```
wine regedit input-style-root.reg
```
다음은 레지스트리가 병합된 후에 터미널에서 `wine regedit`을 명령해보면 다음과
같습니다.

![InputStyle](/images/input-style-root.png)

레지스트리가 잘 병합되었으니 이제 카카오톡을 사용하시면 되겠습니다. 다음은 nimf 로
입력하는 모습입니다.

<div style="text-align:center"> 
  <video id="video1" style="max-width:100%;" controls>
    <source src="/videos/input-style-root.mp4" type="video/mp4">
귀하의 웹 브라우저가 HTML5 비디오를 지원하지 않습니다.
  </video>
</div>
