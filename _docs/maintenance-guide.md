---
layout: post
title: Maintenance Guide
date: 2019-07-22 00:52:13 +0900
---

## What to check when updating the version

### 1. Update translations

```
git clone https://gitlab.com/nimf-i18n/nimf.git
cd nimf
./autogen.sh
cd po
make nimf.pot
```

Check `po/*` files.

### 2. Modify version in `configure.ac`

```
AC_INIT (nimf, YYYY.mm.dd)
...
LIBNIMF_LT_VERSION=C:R:A
```

### 3.  Update `modules/clients/qt5/im-nimf-qt5.cpp`

LT major version is C - A.

```
    libnimf    = dlopen ("libnimf.so.1",        RTLD_LAZY);
    libglib    = dlopen ("libglib-2.0.so.0",    RTLD_LAZY);
    libgobject = dlopen ("libgobject-2.0.so.0", RTLD_LAZY);
    libgio     = dlopen ("libgio-2.0.so.0",     RTLD_LAZY);
```
Check the version of the libraries that are arguments of `dlopen()`.

### 4. Update `debian/changelog`

```
dch -R
```

### 5. Update distro specific files

```
archlinux/PKGBUILD
debian/*
specs/centos.spec
specs/fedora.spec
specs/opensuse.spec
```
