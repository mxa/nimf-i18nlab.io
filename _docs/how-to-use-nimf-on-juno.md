---
layout: post
title: elementary OS 5.0 Juno 에서 nimf 사용하기
date: 2019-03-30 14:40:00 +0900
---

elemntary os 5.0 juno 에서 nimf 를 사용할 때, 표시기가 나타나지 않는 문제가
있습니다. 이 문제를 해결하기 위해 터미널을 열은 후 다음 명령을 수행합니다.

```
sudo sed -i '/OnlyShowIn/d' /etc/xdg/autostart/indicator-application.desktop

wget http://ppa.launchpad.net/elementary-os/stable/ubuntu/pool/main/w/wingpanel-indicator-ayatana/wingpanel-indicator-ayatana_2.0.3+r27+pkg17~ubuntu0.4.1.1_amd64.deb

sudo dpkg -i wingpanel-indicator-ayatana_2.0.3+r27+pkg17~ubuntu0.4.1.1_amd64.deb
```

그 후 로그아웃, 로그인 하시면 되겠습니다.

감사합니다.
