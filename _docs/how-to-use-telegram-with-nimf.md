---
layout: post
title: nimf 와 텔레그램 사용하기
date: 2019-03-16 03:49:00 +0900
---

[텔레그램 공식 홈페이지](https://github.com/telegramdesktop/tdesktop/releases)에서
텔레그램을 다운받아 사용할 경우 nimf 가 작동되지 않는 경우가 있습니다. 이러한 경우
리눅스 배포판 공식 저장소에 있는 텔래그램을 사용하시면 되겠습니다.

### 데비안, 우분투 계열 배포판

```
sudo apt update
sudo apt install telegram-desktop
```

### 아치 리눅스, 만자로 리눅스 계열 배포판

```
pacman -Sy
pacman -S telegram-desktop
```
