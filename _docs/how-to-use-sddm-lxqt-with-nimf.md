---
layout: post
title: nimf 와 sddm, lxqt 사용하기
date: 2019-07-22 00:51:10 +0900
---

nimf 와 lxqt 를 사용하면 nimf 가 작동이 안 되는 경우가 있습니다.  
그래서 확인을 해봤는데, 로그인할 때 디스플레이 관리자로 데비안/우분투 패키지에 있는 sddm 을
사용하면, `.xprofile` 설정을 읽지 않아 환경 변수 설정이 되지 않아서 nimf 가 작동되지 않았던
것입니다.

공식 sddm 은 `.xprofile` 을 불러오는데, sddm 데비안/우분투 패키지는 `.xprofile` 을 불러오지
않는 버그가 있습니다. 아래 참고.

> Sourcing .xprofile is missing from /etc/sddm/Xsession  
> [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=931485](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=931485)

이 문제를 해결하기 위해서는 아래 설명을 따라 주십시오.

## 환경 변수 설정

`nimf-settings` 를 실행하셔서 `환경 변수 설정` 옵션을 켜주세요.  
*만약 다른 입력기를 사용하시려면 `환경 변수 설정` 옵션을 반드시 꺼주셔야 합니다.*

![환경 변수 설정 옵션](/images/ko/setup-environment-variables.png)

터미널을 열어서 아래 명령을 수행해 주세요.

```
wget https://raw.githubusercontent.com/sddm/sddm/master/data/scripts/Xsession
sudo install Xsession /etc/sddm/Xsession
rm Xsession
im-config -n nimf
```

그리고 나서 다시 로그인하시면 nimf 를 바로 사용하실 수 있습니다.
