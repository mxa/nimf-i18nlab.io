---
layout: post
title: How to use Telegram with Nimf
date: 2019-03-20 11:05:00 +0900
permalink: /docs/en/how-to-use-telegram-with-nimf
---

Nimf may not work if you download a telegram from the
[official website of Telegram](https://github.com/telegramdesktop/tdesktop/releases).
In this case, you can use the telagram in the Linux distribution official
repository.

### Debian, Ubuntu-like distribution

```
sudo apt update
sudo apt install telegram-desktop
```

### Arch Linux, Manjaro-like Linux distribution

```
pacman -Sy
pacman -S telegram-desktop
```
