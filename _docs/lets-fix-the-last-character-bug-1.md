---
layout: post
title: "끝글자 버그를 고칩시다 1 - ibus"
date: 2019-06-17 13:51:07 +0900
---

## 이 글의 기획 의도

리눅스 사용하다보면 각종 어플에 발생하는 끝글자 버그 때문에 멘붕이 오는 경우가 많습니다.  
이 글의 첫번째 기획 의도는 끝글자 버그를 고치는 것이고  
두번째 기획 의도는 끝글자 버그를 고치는 방법을 알려드리는 것입니다.  
그동안 보고 받은 어플들을 상대로 끝글자 버그를 고쳐 나가는 방법을 알려드리겠습니다.

## ibus 끝글자 버그

ibus 입력기 자체에 있는 끝글자 버그는 ibus 설계 결함으로 발생하는 것이라, 패치로 버그 회피는
가능하지만 설계 변경 없이 근본적으로 고치는 것은 입력기를 새로 만드는 것 이상으로 매우 어렵습니다.
하지만 과거 어떻게든 ibus 를 사용하기 위해 ibus 프로젝트에도 패치를 보낸 바 있고 수용되지 않았고,
그래서 저는 nimf 를 만들어 사용 중입니다. nimf 를 ibus2 란 이름으로 ibus 프로젝트에 패치를 보낼
수도 없고 ㅎㅎ  
2015년에 ibus 를 고치는 게 너무 어려워서 GG치고 nimf 를 만들게 된 것입니다.  
[https://github.com/ibus/ibus/issues/1282#issuecomment-104839603](https://github.com/ibus/ibus/issues/1282#issuecomment-104839603)  
그래서 ibus 의 끝글자 버그를 해결하는 방법은 nimf 를 사용하는 것입니다.  

### poedit 끝글자 버그

어플에 끝글자 버그가 있습니다. 그래서 확인을 해보니

```
hodong@debian:~/Downloads/poedit-2.2.1$ export G_MESSAGES_DEBUG=nimf
hodong@debian:~/Downloads/poedit-2.2.1$ poedit
(net.poedit.Poedit:29943): nimf-DEBUG: 15:39:07.312: im-nimf.c:565: im_module_init
(net.poedit.Poedit:29943): nimf-DEBUG: 15:39:07.312: im-nimf.c:586: im_module_create
(net.poedit.Poedit:29943): nimf-DEBUG: 15:39:07.312: im-nimf.c:325: nimf_gtk_im_context_new
(net.poedit.Poedit:29943): nimf-DEBUG: 15:39:07.312: im-nimf.c:527: nimf_gtk_im_context_class_ini
```

입력 방법으로 gtk im 을 사용합니다. poedit 소스코드를 확인해보니 `GtkIMContext`, `gtk_im_*`
이 검색되지 않습니다. wxWidgets 을 사용하여 만든 것으로 보이며, `libwxgtk3.0-gtk3-0v5` 를
사용하는 걸로 나오네요. 이 라이브러리를 사용하는 어플로 4pane 이 있습니다.

![wxgtk3](/images/wxgtk3.png)

4pane 를 사용해보니 마찬가지 현상이 발생합니다. 따라서 `libwxgtk3.0-gtk3-0v5` 가 의심이 되며 코드를 확인해보겠습니다.

---

다음 글에서 계속됩니다. [끝글자 버그를 고칩시다 2 - wxWidgets](/docs/lets-fix-the-last-character-bug-2/)
