---
layout: page
title: History
permalink: /history/
---

<table>
<tr><td style="vertical-align:top">2019.01.21</td><td>Support for xkb options</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td style="vertical-align:top">2018.12.05</td><td>Support multiple monitors</td></tr>
<tr><td style="vertical-align:top">2018.11.27</td><td>Support for HiDPI displays</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td style="vertical-align:top">2017.03.25</td><td>Support for Wayland</td></tr>
<tr><td style="vertical-align:top">2017.02.19</td><td>Support multi-user environment</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td style="vertical-align:top">2016.05.29</td><td>Added nimf-settings</td></tr>
<tr><td style="vertical-align:top"><strong>2016.04.26</strong></td><td><strong>Nimf Initial Release</strong></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td style="vertical-align:top"><strong>2015.12.29</strong></td><td><strong>Renamed to Nimf</strong></td></tr>
<tr><td style="vertical-align:top"><strong>2015.10.09</strong></td><td><strong>Dasom Initial Release</strong></td></tr>
<tr><td style="vertical-align:top"><strong>2015.05.23</strong></td><td><strong>Redesigned to client-server modular architecture and renamed to Dasom</strong></td></tr>
<tr><td style="vertical-align:top">2015.02.02</td><td>Started development of Modu input method</td></tr>
<tr><td style="vertical-align:top"><strong>2015.01.22</strong></td><td><strong>Modular design initiated</strong></td></tr>
</table>
