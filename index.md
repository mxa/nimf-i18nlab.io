---
layout: default
---

# Nimf

Nimf is a lightweight, accurate and extensible input method framework
which provides a blueprint for the future of input method frameworks.
See [Architecture](https://nimf-i18n.gitlab.io/architecture).

## Lightweight

Nimf operates in singleton mode by default and consumes less memory.  
One server performs multiple roles.  
It is fast enough to use in a virtual machine because inter-process
communication is minimized.

## Extensible

Nimf consists of modules. You can add your own modules.

## Accurate

Nimf works in the **synchronous** mode and has no side effects.
There are no last letter bugs.

## Robust

Nimf has been rigorously tested in various environments.
